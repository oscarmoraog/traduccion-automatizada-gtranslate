from selenium import webdriver
from selenium.common.exceptions import NoSuchWindowException, ErrorInResponseException, TimeoutException, WebDriverException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from datetime import datetime
import polib
import pyperclip
import requests
import time
import sys
import os

def parse_string(text):
    #Replace the following characters in the text to match with Google Translate syntax
    special_characters = (
            ("%", "%25"),
            (" ", "%20"),
            (",", "%2C"),
            ("?", "%3F"),
            ("\n", "%0A"),
            ('\"', "%22"),
            ("<", "%3C"),
            (">", "%3E"),
            ("#", "%23"),
            ("|", "%7C"),
            ("&", "%26"),
            ("=", "%3D"),
            ("@", "%40"),
            ("#", "%23"),
            ("$", "%24"),
            ("^", "%5E"),
            ("`", "%60"),
            ("+", "%2B"),
            ("\'", "%27"),
            ("{", "%7B"),
            ("}", "%7D"),
            ("[", "%5B"),
            ("]", "%5D"),
            ("/", "%2F"),
            ("\\", "%5C"),
            (":", "%3A"),
            (";", "%3B")
        )

    for pair in special_characters:
        text = text.replace(*pair)    

    return text

def timing(driver):
    # Use Navigation Timing API to calculate the timing for each translation
    navigationStart = driver.execute_script("return window.performance.timing.navigationStart / 1000")
    responseStart = driver.execute_script("return window.performance.timing.responseStart  / 1000")
    domComplete = driver.execute_script("return window.performance.timing.domComplete  / 1000")
    
    # Calculate the backend and frontend performance 
    backEndTime = responseStart - navigationStart
    frontEndTime = domComplete - responseStart
    
    
    return backEndTime, frontEndTime

def translatePO(po, sl, tl, op):   
    
    # Final Entry ID
    finalPOEntry = "customFinalPOFile$%4558461"
    # finalEntry condition
    exits = False
    # Counting entries
    count = 0

    # Counting entries and checking if it has Final Entry ID
    for entry in po:
        count +=1
        if entry.msgid == finalPOEntry:
            exits = True
    totalCount = count

    # Append Final Entry ID in case it doesn't exist
    if exits == False:
        entry = polib.POEntry(msgid=finalPOEntry)
        po.append (entry)
    
    # Parsing data to google Translate, create link, send link 
    id = 0
    for entry in po:
        id +=1
        if entry.msgid == finalPOEntry:
            print(f'-------------- Language {tl} 100% Translated ----------')
            break
        if not entry.msgstr:
            # Parsing text to google translate format
            text = parse_string(entry.msgid)
            # Count of characteres - Max 5.000 for Google Translate (Need to be improved)
            lenText = len(text)

            #In case of more than 5.000 Characteres aproximatelly can't be translated and continue.
            if lenText > 6000:
                print(
                    f'Error Entry {id} --> {entry.occurrences}\n'
                    f'Very big text {lenText} characters, try to translate manually.\n'
                    f'Text Translated --> {po.percent_translated()}%   |   {id} of {totalCount}\n')
                continue
            # Create link to Google Translate request
            link = f"https://translate.google.com/?sl={sl}&tl={tl}&text={text}&op={op}"
            # Send request to Google Translate
            entry.msgstr = translate(link)
            # Define occurrences to appear in the po file
            entry.occurrences = entry.occurrences
            # Save po file
            po.save()
            print(f'Text Translated --> {po.percent_translated()}%   |   {id} of {totalCount}\n')
        else:
            print(f'Entry {id} already translated of {totalCount - 1}')       


# Find unparsed texts with \n tag for msgsstr
# Check if msgid and msgstr have \n in more than one line entries
    po = str(po)
    # obtiene la ubicación del primer string
    num = po.find('msgstr ""')
    param = str('msgstr ""\n' + '"\\n"')
    count = len(po)
    id = 0
    new_po = po[:num]
    # Find unparsed texts with \n tag for msgstr
    while True:
        old_num = num
        num += 1
        num = po.find('msgstr ""', num)
        id += 1  
        if not id == 1:
            append = (po[old_num + 15 : num])
            new_po = new_po + append 
        else:
            append = (po[old_num : num])
            new_po = new_po + append 
        # Count the found coincidences
        id += 1     
        if not id == 1:
            compare = (po[num : num + 15])
            if not compare == param:
                append = compare.replace('msgstr ""', param + compare[num +12 : num + 15], 1)
                new_po = new_po + append
        if num < 0:
            new_po = new_po + po[ old_num : count ]
            break

    return new_po

def translate(link):
    # Create an instance of the Chrome browser
    # (remember to adapt the executable_path to your machine environment)
    driver = webdriver.Chrome(executable_path=r"/usr/bin/chromedriver")
    print("Opening Google Translate")
         
    # open the link in the browser
    try:
        driver.get(link)
        try:
            # Wait until the content_copy button will be able.
            WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.XPATH, f'//*[text()="content_copy"]')))
            backEndTime, frontEndTime = timing(driver)
            print(f'Page is ready! {"%.2f" % frontEndTime} segundos')
        except TimeoutException:
            print(f'Error --> Loading took too much time! FrontEnd: {"%.2f" % frontEndTime} | BackEnd: {"%.2f" % backEndTime} segundos')
            sys.exit(1)
    # Managing errors
    except ErrorInResponseException:
        print("Error --> There was an error from server side...check internet connection")
        sys.exit(1)
    except NoSuchWindowException:
        print("Error --> The Chrome windows was closed... Please try again")
        sys.exit(1)
    except TimeoutException:
        print("Error --> Timeout error.")
        sys.exit(1)
    except WebDriverException:
        print("Error --> Chrome not reachable, check your internet connection")
        sys.exit(1)
    except:
        print("Unknow error, please try again")
        sys.exit(1)

    # Find the copy translation button and click on it
    try:
        driver.find_element_by_xpath(f'//*[text()="content_copy"]').click()
    # Managing errors
    except ErrorInResponseException:
        print("Error --> There was an error from server side...check internet connection")
        sys.exit(1)
    except NoSuchWindowException:
        print("Error --> The Chrome windows was closed... Please try again")
        sys.exit(1)
    except TimeoutException:
        print("Error --> Timeout error.")
        sys.exit(1)
    except WebDriverException:
        print("Error --> Chrome not reachable")
        sys.exit(1)
    except:
        print("Unknow error, please try again")
    
    # paste the translation saved in the clipboard to a variable
    trans = pyperclip.paste()

    # close the browser
    driver.quit()
    
    return trans

def search_google_trans(sl=None, tl=None, url=None, fileType=None, customName=None):
    
    # operation for Google Translate link
    op = "translate"
    
    # Translating links http
    if fileType == "http":
        url = requests.get(url).text

    # Translating .txt (Plain text)
    elif fileType == ".txt":
        with open(url, encoding="UTF-8") as file:
            url = file.read()
    
    # Translating .po files (Parsing data)
    elif fileType == ".po":
        print("Parsing Data of .po file\n")
        po = polib.pofile(url)
        trans = translatePO(po, sl, tl, op)
        # Remove the finalEntry
        trans = trans[ : -60 ]
    # Check if user added Custom Name to their files
    if not customName:
        customName = tl

    # Create subdirectory "./translations/fileType/targetLanguage", if it doesn't exist
    if fileType == ".txt":
        if not os.path.exists(f'./translations/txt/{tl}'):
            os.makedirs(f'translations/txt/{tl}', exist_ok=True)
            print(f'Folder Translations/txt/{tl} created')

    elif fileType == ".po":
        if not os.path.exists(f'./translations/po/{tl}'):
            os.makedirs(f'translations/po/{tl}', exist_ok=True)
            print(f'Folder Translations/po/{tl} created')

    elif fileType == "http":
        if not os.path.exists(f'./translations/http/{tl}'):
            os.makedirs(f'translations/http/{tl}', exist_ok=True)
            print(f'Folder Translations/http/{tl} created')
    else:
        print("This file type is not accepted")

    # Get the local iso 8601 datetime without microseconds
    current_datetime = datetime.now().replace(microsecond=0)
    current_datetime = current_datetime.isoformat().replace(":", "-")

    # create filename for each kind of file
    if fileType == ".po":
        filename = os.path.join(f'translations/po/{tl}', customName + '.po')
        
    elif fileType == ".txt":
        filename = os.path.join(f'translations/txt/{tl}', current_datetime + '_' + customName + '.txt')
    
    elif fileType == "http":
        filename = os.path.join(f'translations/http/{tl}', current_datetime + '_' + customName + '.html')
    
    else:
        print("This file type is not accepted")
    
    # create .txt file to save the translation
    #if not fileType == ".po":
    with open(filename, "w", encoding="UTF-8") as file:
        file.write(trans)
        print(f'File created in path {filename}\n')
    
if __name__ == "__main__":
    sl = 'es'
    languages = ["pt"]#, "es", "eo", "la", "tr", "ko", "ja"]
    #url = "https://google.com/"
    url = "/home/oscarmoraog/Desktop/Yassdance_/prueba_translation/test.po"
    file_found = False
    fileType = None
    customName = "django"

    for language in languages:
        # Check if file is HTTP
        if url.startswith('http'):
            print(f'The text to translate is a Link.\n')
            fileType = "http"
        else:
            # exit the function if file doesn't exits
            if not os.path.isfile(url):
                print("The selected file does not exist\n")
                break
            else:
                if not file_found:
                    print('File Found.')
                    print(f'Languages to translate: {languages}')
                    file_found = True
                # exit the function if file is empty
                if not fileType:
                    if os.stat(url).st_size == 0:
                        print('The selected file is empty\n')
                        break
                    # Checking if is a .txt
                    if url.lower().endswith(".txt"):
                        fileType = ".txt"
                        print('The selected file is a .txt\n')

                    # Checking if is a .po
                    elif url.lower().endswith(".po"):
                        print('The selected file is a .po\n')
                        fileType = ".po"
                    else:
                        print("Error --> Unrecognized file type, can't be translated\n")
                        break
        print(f'---------------- Translating to {language} ----------------')
        search_google_trans(sl, language, url, fileType, customName)
